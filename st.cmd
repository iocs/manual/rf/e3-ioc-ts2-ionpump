require gammaspc
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P1", "TS2-010RFC:RFS01:")
epicsEnvSet("P2", "TS2-010RFC:RFS02:")

epicsEnvSet("R1", "VacPSComm-110:")
epicsEnvSet("IP1", "ionpump-rfs01-110.tn.esss.lu.se:23")
epicsEnvSet("PORT1", "GAMMA1")

epicsEnvSet("R2", "VacPSComm-120:")
epicsEnvSet("IP2", "ionpump-rfs01-120.tn.esss.lu.se:23")
epicsEnvSet("PORT2", "GAMMA2")

epicsEnvSet("R3", "VacPSComm-110:")
epicsEnvSet("IP3", "ionpump-rfs02-110.tn.esss.lu.se:23")
epicsEnvSet("PORT3", "GAMMA3")

epicsEnvSet("R4", "VacPSComm-120:")
epicsEnvSet("IP4", "ionpump-rfs02-120.tn.esss.lu.se:23")
epicsEnvSet("PORT4", "GAMMA4")

# System 1 - First instance
iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh","P=$(P1), R=$(R1), IP=$(IP1), PORT=$(PORT1)")

# System 1 - Second instance
iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh","P=$(P1), R=$(R2), IP=$(IP2), PORT=$(PORT2)")

# System 2 - First instance
iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh","P=$(P2), R=$(R3), IP=$(IP3), PORT=$(PORT3)")

# System 2 - Second instance
iocshLoad("$(gammaspc_DIR)/gammaSpce.iocsh","P=$(P2), R=$(R4), IP=$(IP4), PORT=$(PORT4)")
